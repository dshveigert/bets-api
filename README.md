Start bets API

## Scripts

In the project directory, you can run:

### `node index.js`

## API

Runs the API on [http://localhost:7000/api](http://localhost:7000/api)

Sockets will start on [http://localhost:7000/socket-bets](http://localhost:7000/socket-bets)

Open [http://localhost:7000](http://localhost:7000) to view broadcasting data in the browser.
