const express = require('express');
const app = express();
const cors = require('cors')
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = 7000;
const limitCount = 30;
const fasterBetInterval = 1000 / 30;
let betCounter = 0;
const nameSpaceBets = io.of('/socket-bets');
const {bets, fasterBets} = require('./random-bets-generator');
let interval;
let shortInterval;

const whitelist = ['http://localhost:4200', 'http://37.194.18.29:34200']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  optionsSuccessStatus: 200
}

const clearCounter = () => {
  betCounter = 0;
}
const updateCounter = (delta) => {
  betCounter = betCounter + delta;
}

app.use(express.static(path.join(__dirname, '/')));


app.get('/api/bets', cors(corsOptions), (req, res) => {
  console.log('[/api/bets]', req.query, req.params);
  const betsList = bets(betCounter, limitCount);
  updateCounter(limitCount);
  return res.status(200).send({  data: betsList });
});

app.get('/api/bets/clear-counter', cors(), (req, res) => {
  console.log('[/api/bets/clear-counter]', req.query, req.params);
  clearCounter();
  return res.status(200).send({  data: 'OK' });
});

nameSpaceBets.on('connection', () => {
  console.log('New connection [bets]');
  if (interval) {
    clearInterval(interval);
  }

  interval = setInterval(() => {
    const betsList = bets(betCounter, limitCount);
    updateCounter(limitCount);
    nameSpaceBets.emit('bets', betsList);
  }, 1000);

  if (shortInterval) {
    clearInterval(shortInterval);
  }
  shortInterval = setInterval(() => {
    const betsList = fasterBets(betCounter);
    updateCounter(1);
    nameSpaceBets.emit('faster-bets', betsList);
  }, fasterBetInterval);
});

server.listen(port, () => console.log(`Server listens http://localhost:${port}`))
