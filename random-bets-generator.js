const moment = require('moment');
const randomWords = require('random-words');

exports.bets = (betCounter, limit) => {
  const betList = [];
  for (let i = 0; i < limit; i++) {
    betList.push(betGenerator(betCounter, i));
  }
  return betList;
}

exports.fasterBets = (betCounter) => {
  return betGenerator(betCounter);
}

const betGenerator = (betCounter, i = 0) => {
  const randomNumber = Math.random();
  return {
    id: betCounter + i + 1,
    user: randomWords() + (randomNumber * 10).toFixed(),
    time: moment().format(),
    bet: (randomNumber * 0.0001).toFixed(8),
    multiplier: 'X' + randomNumber.toFixed(8),
    game: (randomNumber > 0.7 ? '> ' : '< ') + (randomNumber * 500).toFixed(8),
    out: (randomNumber * 50).toFixed(8),
    profit: (randomNumber > 0.7 ? '+' : '-') + (randomNumber * 25).toFixed(8)
  }
}
